Examples of use
===============

This section provides examples of how to use the package.
You will discover the basic inputs accepted by the command-line interface as well as the configuration possibilities.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   vanilla
   configuration
